# PROJECT STRUCTURE:
#      ProjectRoot
#       |- src       # Source files. Can contain subdirectories.
#       |- include   # Header files. Can contain subdirectories.
#       |- lib       # Project specific libraries.
#       |- obj       # Intermediate (object) files. Mirrors source tree.
#       |- res       # Project resources.
#       |- exe       # Executable. Built with 'make'.
#       |- test      # Test source files.
#           |- obj   # Object files of test sources. Mirrors test source tree.
#           |- exe   # Test executable. Build with 'make test'.

       
# ------------------------- GENERAL PROJECT SETTINGS ---------------------------
EXECUTABLE = GarageOptimizer
CC = gcc

# Directory structure.
INCLUDE_DIR = include
SRC_DIR = src
OBJ_DIR = obj
LIB_DIR = lib
TEST_DIR = test

# File containing main function definition. Excluded from test compilation.
MAIN_SOURCE_FILE = src/Main.c
# ------------------------------------------------------------------------------


# ------------------------------- COMPILER FLAGS -------------------------------
# Confuguration flags. Append to CPPFLAGS to enable or disable optimizations.
DEBUG_FLAGS = -ggdb -O0
RELEASE_FLAGS = -O2 -DNDEBUG

# Compiler flags.
CPPFLAGS = -Wall -pedantic-errors -MD -I$(INCLUDE_DIR) $(RELEASE_FLAGS)
CFLAGS = -xc -std=c17
# ------------------------------------------------------------------------------


# -------------------------------- LINKER FLAGS --------------------------------
# Linker flags and libraries to link against.
LDFLAGS =
LDLIBS = -lxml2 -lz -llzma -licui18n -licuuc -licudata -ldl

# Link against project specific libraries in 'lib' directory.
LDFLAGS += -L$(LIB_DIR)

# Link against math library. Remove if not needed.
LDFLAGS += -lm
# ------------------------------------------------------------------------------


# ------------------------------ HELPER COMMANDS -------------------------------
# Directory guard. Used to create directory if a file requires it.
DIRECTORY_GUARD = @mkdir -p $(@D)
# ------------------------------------------------------------------------------


# --------------------------- SOURCE AND OBJECT FILES --------------------------
# All source files (search recursively).
SRC := $(shell find $(SRC_DIR) -name '*.c')

# All object files - with paths from project root.
OBJ = $(SRC:$(SRC_DIR)/%.c=$(OBJ_DIR)/%.o)

# Make dependency files - with paths from the project root.
DEP = $(OBJ:.o=.d)
# ------------------------------------------------------------------------------


# -------------------------------- PHONY RULES ---------------------------------
# Special words. Ignore files with those names.
.PHONY: all clean test purge
# ------------------------------------------------------------------------------


# ------------------------------- DEFAULT TARGET -------------------------------
# Build the executable. 
all: $(EXECUTABLE)

# Linking.
$(EXECUTABLE): $(OBJ)
	$(CC) $(LDFLAGS) $^ $(LDLIBS) -o $@ 

# Compiling *.c files.
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(DIRECTORY_GUARD)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@
# ------------------------------------------------------------------------------


# ---------------------------------- TESTING -----------------------------------
# Test sources (only test sources) (search recursively).
TEST_SRC := $(shell find $(TEST_DIR) -name '*.c')

# Test object files (only test files).
TEST_OBJ = $(TEST_SRC:$(TEST_DIR)/%.c=$(TEST_DIR)/$(OBJ_DIR)/%.o)

# Test make dependency files (only test files).
TEST_DEP = $(TEST_OBJ:.o=.d)

# Object file containing 'main()' function.
MAIN_OBJ = $(MAIN_SOURCE_FILE:$(SRC_DIR)/%.c=$(OBJ_DIR)/%.o)

# Linker flags specific for testing. 
TEST_LINKER_FLAGS := $(shell pkg-config --cflags --libs check)

# Build the test executable. 
test: $(TEST_DIR)/$(EXECUTABLE)

# Linking the test executable. Filter out object file that contains 'main()' 
# definition containing main since testing framework provides entry point.
$(TEST_DIR)/$(EXECUTABLE): $(filter-out $(MAIN_OBJ), $(OBJ)) $(TEST_OBJ)
	$(CC) $(LDFLAGS) $^ $(LDLIBS) $(TEST_LINKER_FLAGS) -o $@ 

# Compile test *.c sources.
$(TEST_DIR)/$(OBJ_DIR)/%.o: $(TEST_DIR)/%.c
	$(DIRECTORY_GUARD)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@
# ------------------------------------------------------------------------------


# --------------------------------- CLEANING -----------------------------------
# Remove object files.
clean:
	$(RM) $(OBJ) $(DEP) $(TEST_OBJ) $(TEST_DEP)

# Remove object directories and executable files.
purge:
	$(RM) $(OBJ_DIR) $(TEST_DIR)/$(OBJ_DIR) -r
	$(RM) $(TEST_DIR)/$(EXECUTABLE) $(EXECUTABLE)
# ------------------------------------------------------------------------------


# ---------------------------------- OTHER -------------------------------------
# Make source file recompile if header file that it includes has changed. This
# requires -MD flag passed to the compiler.
-include $(OBJ:.o=.d)
# ------------------------------------------------------------------------------
