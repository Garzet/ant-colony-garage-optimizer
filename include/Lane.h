#ifndef LANE_H
#define LANE_H

typedef struct {
    int ID;
    int length;    /* Capacity of the lane. */
    int n_blocked; /* Number of blocked lanes. */
    int* blocked;  /* Contains IDs of blocked lanes. */
} Lane;


void free_lane(Lane* l);

#endif
