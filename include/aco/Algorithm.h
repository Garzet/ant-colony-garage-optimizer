#ifndef ALGORITHM_H
#define ALGORITHM_H

#include <time.h>
#include <stdbool.h>

#include "aco/Population.h"
#include "aco/Pheromones.h"
#include "Parameters.h"
#include "Instance.h"
#include "Fitness.h"


typedef struct {
    time_t start_time; /* Algorithm start time. */
    int iteration;     /* Current algorithm iteration. */
    Solution bs;       /* Best found solution. */
    Population p;      /* Algorithm population. */
    Pheromones phr;    /* Algorithm pheromone state. */
    double *etas;      /* Algortihm eta values. */
} State;


/* Runs the Ant Colony Optimization algorithm. When termination criteria is met,
 * the best found solution is stored into the 'bs' array (array must be the size
 * of the solution). */
void start_aco_algorithm(const Instance* inst, const Parameters* pars, int* bs);

#endif
