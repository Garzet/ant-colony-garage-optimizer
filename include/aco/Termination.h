#ifndef TERMINATION_H
#define TERMINATION_H

#include <stdbool.h>

#include "Parameters.h"
#include "Algorithm.h"

/* Returns true if any of the termination criteria has been met. */
bool termination_criteria_met(const State* s, const Parameters* p);

#endif
