#include "Tests.h"

#include <malloc.h>

#include "aco/Population.h"
#include "aco/Fitness.h"


START_TEST (f1_calculation)
{
    /* Initialize population of one solution with five dimensions. */
    Population pop; 
    init_population(&pop, 1, 5);

    /* Make lanes hold the following series: [1, 0, 2, 2]. Note that zero means
     * that the lane is empty. */
    pop.sols[0].sol_data[0] = 2; /* Vehicle: 0, Lane: 2, Series: 2 */
    pop.sols[0].sol_data[1] = 3; /* Vehicle: 1, Lane: 3, Series: 2 */
    pop.sols[0].sol_data[2] = 0; /* Vehicle: 2, Lane: 0, Series: 1 */
    pop.sols[0].sol_data[3] = 0; /* Vehicle: 3, Lane: 0, Series: 1 */
    pop.sols[0].sol_data[4] = 0; /* Vehicle: 4, Lane: 0, Series: 1 */

    Instance x;
    load_instance_data(&x, "test/res/test_instance.txt");

    /* Initialize fitenss calculation area (global variables). */
    init_fitness_calculation_area(&x);

    decode_solution(&pop.sols[0], &x); /* Decode solution. */
    ck_assert_int_eq(f1(&x), 1); /* Only first and third lane differ. */

    /* Free fitness calculation area and population data. */
    free_fitness_calculation_area();
    free_population(&pop);
    free_instance_data(&x);
}
END_TEST


START_TEST (f2_calculation)
{
    /* Initialize population of two solutions with three dimensions. */
    Population pop; 
    init_population(&pop, 2, 3);
    for (int i = 0; i < pop.pop_size * pop.sol_size; i++) {
        pop.pop_data[i] = i + 1;
    }

    /* First solution looks like this: {1, 2, 3}. */
    pop.sols[0].sol_data[0] = 2; /* Make it look like this: {2, 2, 3}. */
    ck_assert_int_eq(f2(pop.sols + 0, pop.sol_size), 2); /* Expect 2 uniques. */

    /* Second solution looks like this: {4, 5, 6}. */
    ck_assert_int_eq(f2(pop.sols + 1, pop.sol_size), 3); /* Expect 3 uniques. */

    free_population(&pop);
}
END_TEST


START_TEST (f3_calculation)
{
    /* Initialize population of two solutions with five dimensions. */
    Population pop; 
    init_population(&pop, 2, 5);

    Instance x;
    load_instance_data(&x, "test/res/test_instance.txt");

    init_fitness_calculation_area(&x);

    /* Initialize first solution: [1, 1, 0, 3, 3}. */
    pop.sols[0].sol_data[0] = 1; /* 182 - 42       = 140  */
    pop.sols[0].sol_data[1] = 1; /* 140 - 48 - 0.5 = 91.5 */
    pop.sols[0].sol_data[2] = 0; /* 140 - 42       = 98   */
    pop.sols[0].sol_data[3] = 3; /* 144 - 53       = 91   */
    pop.sols[0].sol_data[4] = 3; /*  91 - 37 - 0.5 = 53.5 */

    /* Lane 0: 98 left; Lane 1: 91.5 left; Lane 3: 53.5 left. */
    decode_solution(&pop.sols[0], &x); /* Decode solution. */
    ck_assert_double_eq_tol(f3(&x), 91.5 + 98 + 53.5, 10e-12);

    /* Initialize first solution: [1, 1, 1, 1, 3}. */
    pop.sols[1].sol_data[0] = 1; /* 182  - 42       =  140  */
    pop.sols[1].sol_data[1] = 1; /* 140  - 48 - 0.5 =  91.5 */
    pop.sols[1].sol_data[2] = 1; /* 91.5 - 42 - 0.5 =  49   */
    pop.sols[1].sol_data[3] = 1; /* 49   - 53 - 0.5 = -4.5  */
    pop.sols[1].sol_data[4] = 3; /* 144  - 37       =  107  */

    /* Lane 1: -4.5 left; Lane 3: 107 left. */
    decode_solution(&pop.sols[1], &x); /* Decode solution. */
    ck_assert(isinf(f3(&x)));
    
    free_fitness_calculation_area();
    free_population(&pop);
    free_instance_data(&x);
}
END_TEST


START_TEST (g1_calculation)
{
    /* Initialize population of one solution with five dimensions. */
    Population pop; 
    init_population(&pop, 1, 5);

    /* Make lanes hold the following schedules: [{6,0,6}, {}, {1}, {4}]. */
    pop.sols[0].sol_data[0] = 2; /* Vehicle: 0, Lane: 2, Schedule: 1 */
    pop.sols[0].sol_data[1] = 3; /* Vehicle: 1, Lane: 3, Schedule: 4 */
    pop.sols[0].sol_data[2] = 0; /* Vehicle: 2, Lane: 0, Schedule: 0 */
    pop.sols[0].sol_data[3] = 0; /* Vehicle: 3, Lane: 0, Schedule: 6 */
    pop.sols[0].sol_data[4] = 0; /* Vehicle: 4, Lane: 0, Schedule: 6 */
    
    Instance x;
    load_instance_data(&x, "test/res/test_instance.txt");
    
    /* Initialize fitenss calculation area (global variables). */
    init_fitness_calculation_area(&x);

    decode_solution(&pop.sols[0], &x); /* Decode solution. */
    ck_assert_int_eq(g1(&x), 0);

    /* Make lanes hold the following schedules: [{6,6,6}, {}, {1}, {4}]. */
    x.vehicles[2].schedule = 6; /* Vehicle: 2, Lane: 0, Schedule: 6 */
    ck_assert_int_eq(g1(&x), 2);
    
    /* Free fitness calculation area and population data. */
    free_fitness_calculation_area();
    free_population(&pop);
    free_instance_data(&x);
}
END_TEST


START_TEST (g2_calculation)
{
    /* Initialize population of one solution with five dimensions. */
    Population pop; 
    init_population(&pop, 1, 5);

    /* Make lanes hold the following schedules: [{6,0,6}, {}, {1}, {4}]. */
    pop.sols[0].sol_data[0] = 2; /* Vehicle: 0, Lane: 2, Schedule: 1 */
    pop.sols[0].sol_data[1] = 3; /* Vehicle: 1, Lane: 3, Schedule: 4 */
    pop.sols[0].sol_data[2] = 0; /* Vehicle: 2, Lane: 0, Schedule: 0 */
    pop.sols[0].sol_data[3] = 0; /* Vehicle: 3, Lane: 0, Schedule: 6 */
    pop.sols[0].sol_data[4] = 0; /* Vehicle: 4, Lane: 0, Schedule: 6 */
    
    Instance x;
    load_instance_data(&x, "test/res/test_instance.txt");
    
    /* Initialize fitenss calculation area (global variables). */
    init_fitness_calculation_area(&x);

    decode_solution(&pop.sols[0], &x); /* Decode solution. */
    ck_assert_int_eq(g2(&x), 0);

    /* Make lanes hold the following schedules: [{0,6}, {6}, {6}, {4}]. */
    pop.sols[0].sol_data[4] = 1; /* Vehicle: 4, Lane: 0, Schedule: 6 */
    x.vehicles[0].schedule = 6;  /* Vehicle: 0, Lane: 2, Schedule: 6 */
    decode_solution(&pop.sols[0], &x); /* Decode solution. */
    ck_assert_int_eq(g2(&x), 2);
    
    /* Free fitness calculation area and population data. */
    free_fitness_calculation_area();
    free_population(&pop);
    free_instance_data(&x);
}
END_TEST


START_TEST (g3_calculation)
{
    /* Initialize population of one solution with five dimensions. */
    Population pop; 
    init_population(&pop, 1, 5);

    /* Make lanes with departures: [{276,345,360}, {}, {288}, {333}]. */
    pop.sols[0].sol_data[0] = 2; /* Vehicle: 0, Lane: 2, Departure: 288 */
    pop.sols[0].sol_data[1] = 3; /* Vehicle: 1, Lane: 3, Departure: 333 */
    pop.sols[0].sol_data[2] = 0; /* Vehicle: 2, Lane: 0, Departure: 345 */
    pop.sols[0].sol_data[3] = 0; /* Vehicle: 3, Lane: 0, Departure: 360 */
    pop.sols[0].sol_data[4] = 0; /* Vehicle: 4, Lane: 0, Departure: 276 */
    
    Instance x;
    load_instance_data(&x, "test/res/test_instance.txt");
    
    /* Initialize fitenss calculation area (global variables). */
    init_fitness_calculation_area(&x);

    decode_solution(&pop.sols[0], &x); /* Decode solution. */
    /* Differences: Lane 0: 345 - 276 = 69, 360 - 345 = 15, other lanes none. */
    int n_evaluated_pairs;
    ck_assert_int_eq(g3(&x, &n_evaluated_pairs), 10 + 15);
    ck_assert_int_eq(n_evaluated_pairs, 2);
    
    /* Make lanes with departures: [{276,345,360}, {}, {288, 293}, {}]. */
    x.vehicles[1].departure = 293;
    pop.sols[0].sol_data[1] = 2; /* Vehicle: 1, Lane: 2, Departure: 293 */

    /* Differences: Lane 0: 345 - 276 = 69, 360 - 345 = 15 */
    /* Differences: Lane 2: 293 - 288 = 5 */
    decode_solution(&pop.sols[0], &x); /* Decode solution. */
    ck_assert_int_eq(g3(&x, &n_evaluated_pairs), 10 + 15 - 4 * (10 - 5));
    ck_assert_int_eq(n_evaluated_pairs, 3);

    /* Free fitness calculation area and population data. */
    free_fitness_calculation_area();
    free_population(&pop);
    free_instance_data(&x);
}
END_TEST


START_TEST (solution_decoding)
{
    Instance x;
    load_instance_data(&x, "test/res/test_instance.txt");

    Solution s;
    s.sol_data = (int*) malloc(x.n_vehicles * sizeof(int));
    s.sol_data[0] = 2; /* Vehicle: 0, Lane: 2, Departure: 288 */
    s.sol_data[1] = 3; /* Vehicle: 1, Lane: 3, Departure: 333 */
    s.sol_data[2] = 0; /* Vehicle: 2, Lane: 0, Departure: 345 */
    s.sol_data[3] = 0; /* Vehicle: 3, Lane: 0, Departure: 360 */
    s.sol_data[4] = 0; /* Vehicle: 4, Lane: 0, Departure: 276 */

    init_fitness_calculation_area(&x);

    decode_solution(&s, &x); /* Decode solution into the global variable. */
    /* Expected vehicles per lane (decoded solution). */
    int vpl[4][5] = {{ 4,  2,  3, -1, -1},
                     {-1, -1, -1, -1, -1},
                     { 0, -1, -1, -1, -1},
                     { 1, -1, -1, -1, -1}};
    ck_assert_mem_eq(decoded_solution, vpl, 4*5*sizeof(int));
    
    free_fitness_calculation_area();
    free(s.sol_data);
    free_instance_data(&x);
}
END_TEST


START_TEST (total_vehicle_length_calculation)
{
    Instance x;
    load_instance_data(&x, "test/res/test_instance.txt");
    init_fitness_calculation_area(&x);
    
    ck_assert_int_eq(total_vehicle_length, 42 + 48 + 42 + 53 + 37);

    free_fitness_calculation_area();
    free_instance_data(&x);
}
END_TEST


START_TEST (total_lane_length_calculation)
{
    Instance x;
    load_instance_data(&x, "test/res/test_instance.txt");
    init_fitness_calculation_area(&x);
    
    ck_assert_int_eq(total_lane_capacity, 140 + 182 + 123 + 144);

    free_fitness_calculation_area();
    free_instance_data(&x);
}
END_TEST


Suite* fitness_calculation_suite()
{
    Suite* s = suite_create("Fitness calculation suite");
    TCase* tc_core = tcase_create("Core");

    tcase_add_test(tc_core, f1_calculation);
    tcase_add_test(tc_core, f2_calculation);
    tcase_add_test(tc_core, f3_calculation);
    tcase_add_test(tc_core, g1_calculation);
    tcase_add_test(tc_core, g2_calculation);
    tcase_add_test(tc_core, g3_calculation);
    tcase_add_test(tc_core, solution_decoding);
    tcase_add_test(tc_core, total_vehicle_length_calculation);
    tcase_add_test(tc_core, total_lane_length_calculation);

    suite_add_tcase(s, tc_core);
    return s;
}
