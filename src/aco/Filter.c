#include "aco/Filter.h"

#include "aco/Fitness.h"

#include <math.h>
#include <stdio.h>

void filter_probabilities(double* probs, const Instance* inst,
        const Solution* sol, int vi)
{
    if (vi == 0) return; /* Nothing to filter for first vehicle. */

    bool has_feasible_options = false;
    double sum_feasibles = 0.0; /* Sum of feasible probabilities. */
    for (int li = 0; li < inst->n_lanes; li++) {
        double prob = probs[li]; /* Probability that vi will pick lane li. */
        
        if (prob != 0) {                             /* Lane can be picked. */
            sol->sol_data[vi] = li;                  /* Pick that lane. */
            decode_solution(sol, inst);              /* Decode solution. */
            bool feasible = solution_is_valid(inst); /* Check if feasible. */
            if (!feasible) { /* Lane should not be picked. */
                probs[li] = 0;
            } 
            else {           /* Feasible option found. */
                has_feasible_options = true;
                sum_feasibles += probs[li];
            } 
        }
    }

    if (has_feasible_options) {
        /* Normalize feasible probabilities by dividing with the sum. This means
         * that probabilities sum to one. */
        for (int li = 0; li < inst->n_lanes; li++) {
            probs[li] /= sum_feasibles; 
        }
    } else { /* No feasible options for lane. Solution will be unfeasible. */
        double prob = 1.0 / inst->n_lanes; /* Make probabilities equal. */
        for (int li = 0; li < inst->n_lanes; li++) {
            probs[li] = prob;
        }         
    } 
}


bool solution_is_valid(const Instance* inst)
{
    return valid_lane_series(inst) &&   /* Lanes contain one vehicle series. */
           valid_lane_blocking(inst) && /* Lanes do not block each other. */
           !isinf(f3(inst));            /* Lanes are not overfilled. */
}


bool valid_lane_blocking(const Instance* inst)
{
    for (int li = 0; li < inst->n_lanes; li++) { /* Blocking lanes loop. */
        const Lane* blocking = inst->lanes + li; /* Blocking lane pointer. */
        if (blocking->n_blocked < 1) continue;   /* No blocked lanes. */
        if (nvpl[li] < 1) continue;  /* No vehicles in blocking lane. */

        /* Pointer to indices of vehicles in the blocking lane. */
        int* blv = decoded_solution + li * inst->n_vehicles;

        /* Departure time of last vehicle in the blocking lane. */
        int blocking_time = inst->vehicles[blv[nvpl[li] - 1]].departure;

        for (int i = 0; i < blocking->n_blocked; i++) {
            int bi = blocking->blocked[i] - 1; /* Blocked lane index. */

            if (nvpl[bi] < 1) continue;  /* No vehicles in blocked lane. */

            /* Pointer to indices of vehicles in the blocked lane. */
            int* blv2 = decoded_solution + bi * inst->n_vehicles;

            /* Departure time of first vehicle in the blocked lane. */
            int blocked_time = inst->vehicles[blv2[0]].departure;

             
            /* Blocking time greater than blocked - solution is invalid. */
            if (blocking_time >= blocked_time) {
                return false;
            }
        } 
    } 
    return true;
}


bool valid_lane_series(const Instance* inst)
{
    for (int li = 0; li < inst->n_lanes; li++) {
        if (nvpl[li] < 2) continue;  /* Less than two vehicles in lane. */
        
        /* Pointer to indices of vehicles in the lane (lane data). */
        int* ld = decoded_solution + li * inst->n_vehicles;

        /* Series of first vehicle. Other should be the same. */
        int series_ref = inst->vehicles[ld[0]].series;

        /* Compare each following vehicle series with that of first vehicle. */
        for (int i = 1; i < nvpl[li]; i++) {
            if (inst->vehicles[ld[i]].series != series_ref) {
                return false;
            }
        }
    }
    return true;
}
