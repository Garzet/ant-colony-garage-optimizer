#include "aco/Population.h"

#include <malloc.h>
#include <math.h>

#include "aco/Fitness.h"


void init_population(Population* p, int pop_size, int sol_size)
{
    p->pop_data = (int*) malloc(pop_size * sol_size * sizeof(int)); 
    p->pop_size = pop_size;
    p->sol_size = sol_size;

    /* Allocate and initialize solutions. Solutions point to pop_data array. */
    p->sols = (Solution*) malloc(pop_size * sizeof(Solution));
    for (int i = 0; i < pop_size; i++) {
        p->sols[i].sol_data = p->pop_data + i * sol_size; 
        p->sols[i].fitness = -INFINITY;
    }
}


void free_population(Population* p)
{
    free(p->sols);     /* Free solutions array. Does not free and pop_data. */
    free(p->pop_data); /* Free population data. */
}
