#include "aco/Fitness.h"

#include <malloc.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#include "Sorter.h"
#include "aco/Filter.h"

/* Returns first non-empty lane starting from lane with the provided index.
 * Index is inclusive meaning that if lane with provided index is empty, the
 * provided index is returned. Returns negative number such non-empty lane does
 * not exist. Function uses the global decoded solution for calculations. */
static int first_non_empty_lane(int li, int n_lanes);

double evaluate(const Solution* s, const Instance* inst)
{
    /* Decode a solution and place result into global variable. */
    decode_solution(s, inst);

    int    f1_val = f1(inst);
    int    f2_val = f2(s, inst->n_vehicles); /* Number of used lanes. */
    double f3_val = f3(inst);

    int    g1_val = g1(inst);
    int    g2_val = g2(inst);
    int    n_evaluated_pairs;
    int    g3_val = g3(inst, &n_evaluated_pairs);
    
    double p1 = 1.0 / (f2_val - 1);
    double p2 = 1.0 / inst->n_lanes;
    double p3 = 1.0 / (total_lane_capacity - total_vehicle_length); 

    double r1 = 1.0 / (inst->n_vehicles - f2_val);
    double r2 = 1.0 / (f2_val - 1);
    double r3 = 1.0 / (15 * n_evaluated_pairs);

    double max_goal = r1 * g1_val + r2 * g2_val + r3 * g3_val;
    double min_goal = p1 * f1_val + p2 * f2_val + p3 * f3_val;
            
    if (!solution_is_valid(inst)) return 0; /* Check if feasible. */

    return max_goal / min_goal;
}


int f1(const Instance* inst)
{
    int n_diff = 0; /* Number of co-residing lanes with different series. */

    /* Get first and second lane index (lanes to compare). */
    int fli = first_non_empty_lane(0, inst->n_lanes);
    int sli = first_non_empty_lane(fli + 1, inst->n_lanes);
    while (sli > 0) { /* Both non-empty lanes exist. */
        /* First and second lane data. */
        int* fld = decoded_solution + fli * inst->n_vehicles;
        int* sld = decoded_solution + sli * inst->n_vehicles; 

        /* Series of first vehicle in second and first lane. */
        int ser1 = inst->vehicles[fld[0]].series;
        int ser2 = inst->vehicles[sld[0]].series;

        if (ser1 != ser2) n_diff++;

        /* Second lane becomes first and new second lane is found. */
        fli = sli;
        sli = first_non_empty_lane(fli + 1, inst->n_lanes);
    }
    return n_diff;
}


int f2(const Solution* s, int sol_size)
{
    int n_unique = 1;                    /* First element is unique. */
    for (int i = 1; i < sol_size; i++) { /* Iterate over solution. */
       bool is_unique = true;            /* Assume element is unique. */

       /* Iterate until current element and until current element is unique. */
       for (int j = 0; is_unique && j < i; j++) {
            if (s->sol_data[i] == s->sol_data[j]) is_unique = false;
       }
       if (is_unique) n_unique++;
    }
    return n_unique; /* Return number of used lanes. */
}


double f3(const Instance* inst)
{
    double tfs = 0; /* Total free space across all lanes. */
    for (int li = 0; li < inst->n_lanes; li++) { /* Iterate over lanes. */
        if (nvpl[li] < 1) continue;              /* Skip empty lane. */

        int* ld = decoded_solution + li * inst->n_vehicles; /* Lane data. */

        /* Lane free space. Decrease lane length by length of first vehicle. */
        double lfs = inst->lanes[li].length - inst->vehicles[ld[0]].length;

        for (int vil = 1; vil < nvpl[li]; vil++) { /* Vehicles iteration. */
            int vlen = inst->vehicles[ld[vil]].length; /* Vehicle length. */

            lfs -= vlen + 0.5; /* Vehicle length and space between vehicles. */
            if (lfs < 0) return -INFINITY;
        }
        tfs += lfs;
    }
    return tfs;
}


int g1(const Instance* inst)
{
    int n_same = 0; /* Number of co-residing vehicles with same schedules. */
    for (int li = 0; li < inst->n_lanes; li++) { /* Iterate over lanes. */
        if (nvpl[li] < 2) continue; /* Skip lane if no two vehicles in it. */

        int* ld = decoded_solution + li * inst->n_vehicles; /* Lane data. */
        for (int vil = 0; vil < nvpl[li] - 1; vil++) { /* Vehicle iteration. */
            /* First and second vehicle schedule type. */
            int fst = inst->vehicles[ld[vil    ]].schedule;
            int sst = inst->vehicles[ld[vil + 1]].schedule;

            if (fst == sst) n_same++;
        }
    }
    return n_same;
}


int g2(const Instance* inst)
{
    /* Number of co-residing lanes where last vehicle in the first lane has the
     * same schedule type as the first vehicle in the second lane. */
    int n_same = 0; 

    /* Get first and second lane index (lanes to compare). */
    int fli = first_non_empty_lane(0, inst->n_lanes);
    int sli = first_non_empty_lane(fli + 1, inst->n_lanes);
    while (sli > 0) { /* Both non-empty lanes exist. */
        /* First and second lane data. */
        int* fld = decoded_solution + fli * inst->n_vehicles;
        int* sld = decoded_solution + sli * inst->n_vehicles; 

        /* Schedule of last vehicle in the first lane and schedule of first
         * vehicle in the second lane. */
        int sch1 = inst->vehicles[fld[nvpl[fli] - 1]].schedule;
        int sch2 = inst->vehicles[sld[0]].schedule;

        if (sch1 == sch2) n_same++;

        /* Second lane becomes first and new second lane is found. */
        fli = sli;
        sli = first_non_empty_lane(fli + 1, inst->n_lanes);
    }
    return n_same;
}


int g3(const Instance* inst, int* n_evaluated_pairs)
{
    int sum = 0; /* Total sum of rewards and punishments. */
    *n_evaluated_pairs = 0; /* Initialize counter. */
    for (int li = 0; li < inst->n_lanes; li++) {   /* Iterate over lanes. */
        if (nvpl[li] < 2) continue; /* Skip lane if no two vehicles in it. */

        int* ld = decoded_solution + li * inst->n_vehicles; /* Lane data. */
        for (int vil = 0; vil < nvpl[li] - 1; vil++) { /* Vehicle iteration. */
            /* First and second vehicle departure time. */
            int fdt = inst->vehicles[ld[vil    ]].departure;
            int sdt = inst->vehicles[ld[vil + 1]].departure;

            int d = sdt - fdt; /* Difference between departure times. */

            /* Reward or punish according to difference between departures. */
            sum += d < 10 ? -4 * (10 - d) : d > 20 ? 10 : 15; 

            /* Increment evaluated pairs counter. */
            (*n_evaluated_pairs)++;
        }
    }
    return sum;
}


void init_fitness_calculation_area(const Instance* inst)
{
    /* Local shortcuts. */
    int n_lanes = inst->n_lanes;
    int n_vehicles = inst->n_vehicles;

    decoded_solution = (int*) malloc(n_lanes * n_vehicles * sizeof(int));
    /* Initialize decoded solution matrix - each lane is empty. */
    for (int i = 0; i < n_lanes * n_vehicles; i++) {
        decoded_solution[i] = -1; /* Negative vehicle index means empty slot. */
    }

    /* Allocate memory for number of vehicles per lane. */
    nvpl = (int*) malloc(n_lanes * sizeof(int)); 

    /* Calculate sum of capacities of all lanes. */ 
    total_lane_capacity = 0;
    for (int li = 0; li < inst->n_lanes; li++) {
        total_lane_capacity += inst->lanes[li].length;    
    }

    /* Calculate sum of lengths of all vehicles.*/
    total_vehicle_length = 0;
    for (int vi = 0; vi < inst->n_vehicles; vi++) {
        total_vehicle_length += inst->vehicles[vi].length;    
    }
}


void free_fitness_calculation_area()
{
    free (nvpl);
    free(decoded_solution);
}


/* Adds a vehicle with the provided index to the lane with the provided index
 * in the decoded solution. */
static void add_vehicle_to_lane(int vi, int li, int n_vehicles)
{
    /* Pointer to beginning of data for lane (matrix row) with given index.
     * In other words, this is a pointer to the start of vehicles in lane.  */
    int* ld = decoded_solution + li * n_vehicles; 

    while(*ld >= 0) ld++; /* Find first empty slot (negative vehicle index). */

    *ld = vi; /* Add vehicle to the end of the lane (found empty slot). */

    nvpl[li]++; /* Increase number of vehicles in the lane. */
}


void decode_solution(const Solution* s, const Instance* inst)
{
    /* Local shortcuts for instance data. */
    int nv = inst->n_vehicles;
    int nl = inst->n_lanes;

    /* Initialize decoded solution matrix - each lane is empty. */
    for (int i = 0; i < nl; i++) { /* Iterate over matrix rows. */
        /* Only iterate until first negative vehicle index in the row is found.
         * All following indices in the row are surely negative as well so they
         * do not need to be initialized. */
        for (int j = 0; j < nv && decoded_solution[i * nv + j] >= 0; j++) {
            decoded_solution[i * nv + j] = -1;
        }
    }

    /* Inizialize number of vehicles per lane to zero - each lane is empty. */
    memset(nvpl, 0, nl * sizeof(int));

    /* Iterate over solution data and add each vehicle to corresponding lane.
     * This results in decoded solution that still does not have each row sorted
     * by vehicle departure time. The check for elements greater than zero has
     * to be here because if partial solution is sent to decoding, then not all
     * elements have valid values (all past certain index are negative). */
    for (int vi = 0; vi < nv && s->sol_data[vi] >= 0; vi++) {
        add_vehicle_to_lane(vi, s->sol_data[vi], nv);     
    }
    
    /* Sort vehicles in each row (lane) by departure time - ascending. */
    for (int li = 0; li < nl; li++) {
        int* lane_data = decoded_solution + li * nv;     /* Row begnining. */
        if (nvpl[li] > 0) {                              /* Lane not empty. */
            quicksort(lane_data, 0, nvpl[li] - 1, inst); /* Sort. */
        }
    }
}


static int first_non_empty_lane(int li, int n_lanes)
{
    if (li < 0 || li >= n_lanes) return -1;
    for (; nvpl[li] < 1 && li < n_lanes; li++);
    return li < n_lanes ? li : -1;
}
