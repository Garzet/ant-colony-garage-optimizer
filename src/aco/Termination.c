#include "aco/Termination.h"

#include <math.h>

static bool max_iterations_reached(const State* s, const Parameters* p);
static bool target_fitness_reached(const State* s, const Parameters* p);
static bool max_time_reached(const State* s, const Parameters* p);


bool termination_criteria_met(const State* s, const Parameters* p)
{
    /* Returns true if at least one of the criteria is met. */
    return max_iterations_reached(s, p)
        || max_time_reached(s, p)
        || target_fitness_reached(s, p); 
}


static bool max_iterations_reached(const State* s, const Parameters* p)
{
    return p->max_iter > 0 &&        /* Criteria is active. */
        s->iteration >= p->max_iter; /* Max iterations reached. */
}


static bool target_fitness_reached(const State* s, const Parameters* p)
{
    return !isinf(p->target_fitness) &&     /* Criteria is active. */
        s->bs.fitness >= p->target_fitness; /* Fitness reached. */
}


static bool max_time_reached(const State* s, const Parameters* p)
{
    return p->max_time > 0 &&  /* Criteria is active. */
        (int) (time(NULL) - s->start_time) >= p->max_time; /* Time elapsed. */
}
