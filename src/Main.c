#include <stdio.h>
#include <stdlib.h>

#include "Instance.h"
#include "Parameters.h"
#include "aco/Algorithm.h"

int main(void) {
    /* Set seed for random generator. */
    srand(time(NULL));
    /*srand(42);*/

    /* Load parameters from the parameters XML file. */
    Parameters parameters;
    load_parameters(&parameters, "res/parameters.xml");

    /* Load instance data from instance text file. */
    Instance instance;
    load_instance_data(&instance, parameters.instance_filename);

    /* Allocate space for best solution. */
    Solution s;
    s.sol_data = (int*) malloc(instance.n_vehicles * sizeof(int));
    
    /* Run the algorithm. */
    start_aco_algorithm(&instance, &parameters, s.sol_data);

    /* Report on best found solution. */
    printf("Finished aco algorithm!\nBest found solution: ");
    for (int i = 0; i < instance.n_vehicles; i++) {
        printf("%d ", s.sol_data[i]);
    }

    /* Decode and write best solution. */
    init_fitness_calculation_area(&instance); /* Allocate space for decoding. */
    decode_solution(&s, &instance);           /* Decode solution. */
    FILE *fptr = fopen("res/solution.txt", "w");
    for (int li = 0; li < instance.n_lanes; li++) {
        int *ld = decoded_solution + li * instance.n_vehicles;
        for (int vi = 0; vi < instance.n_vehicles && ld[vi] >= 0; vi++) {
            fprintf(fptr, "%d ", ld[vi] + 1); /* Index to ID hence plus one. */
        }
        fprintf(fptr, "\n");
    }
    free_fitness_calculation_area();          /* Free space for decoding. */

    /* Free allocated memory. */
    free(s.sol_data); 
    free_instance_data(&instance);
    free_parameters_data(&parameters);
    
    return 0;
}
